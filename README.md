# Headspace iOS mono-repo

Mono-repository for all-things iOS. Maintained with [Buck].

## First thing

- Install required tools:

        make install_buck

- [xctool] comes bundled with repo.

## Building player

```
make build
```

To generate *Xcode* project:

```
make project
```

## Running player on Simulator

```
make debug
```

## File structure

```
.
├── apps
│   └── player
├── libraries
│   └── viper
├── vendor
│   └── Pods
│       ├── RxCocoa
│       ├── RxSwift
│       └── IGListKit
├── tools
│   ├── xctool
│   └── buck
└── scripts
```

[Buck]: https://buckbuild.com
[xctool]: https://github.com/facebook/xctool
