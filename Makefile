.PHONY: apps build clean debug libraries lint install_buck project player update_pods vendor

BUCK=tools/buck/bin/buck
BUCK_VERSION=2019.01.10.01

APP?=player

apps:
	@$(BUCK) targets //apps/...

build:
	$(BUCK) build //apps/$(APP):$(APP)

clean:
	rm -rf **/*.xcworkspace
	rm -rf **/*.xcodeproj
	$(BUCK) clean

debug:
	$(BUCK) install //apps/$(APP):$(APP) --run --simulator-name 'iPhone XR'

libraries:
	@$(BUCK) targets //libraries/...

lint:
	@cd apps && swiftlint
	@cd libraries && swiftlint

install_buck:
	@scripts/install_buck.sh $(BUCK_VERSION)

project: clean
	$(BUCK) project //apps/$(APP):workspace

player:
	@$(BUCK) project //apps/player:workspace

update_pods:
	@scripts/update_pods.sh

vendor:
	@$(BUCK) targets //vendor/...
