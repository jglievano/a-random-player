import Viper

protocol RootInteractable: Interactable, PlayListener {
  var router: RootRouting? { get set }
  var listener: RootListener? { get set }
}

protocol RootViewControllable: ViewControllable {
  func present(viewController: ViewControllable)
}

final class RootRouter: LaunchRouter<RootInteractable, RootViewControllable>, RootRouting {
  init(interactor: RootInteractable,
       viewController: RootViewControllable,
       playBuilder: PlayBuilder) {
    self.playBuilder = playBuilder
    super.init(interactor: interactor, viewController: viewController)
    interactor.router = self
  }

  override func didLoad() {
    super.didLoad()

    let play = playBuilder.build(with: interactor)
    self.play = play
    attachChild(play)
    viewController.present(viewController: play.viewControllable)
  }

  // MARK: - Private

  private let playBuilder: PlayBuildable
  private var play: Routing?
}
