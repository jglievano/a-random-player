import Viper

protocol RootDependency: Dependency {}
protocol RootDependencyPlay: Dependency {}

final class RootComponent: Component<RootDependency> {
  let rootViewController: RootViewController

  init(dependency: RootDependency, rootViewController: RootViewController) {
    self.rootViewController = rootViewController
    super.init(dependency: dependency)
  }
}

extension RootComponent: PlayDependency {
  var playViewController: PlayViewControllable {
    return rootViewController
  }
}

// MARK: - Builder

protocol RootBuildable: Buildable {
  func build() -> LaunchRouting
}

final class RootBuilder: Builder<RootDependency>, RootBuildable {
  override init(dependency: RootDependency) {
    super.init(dependency: dependency)
  }

  func build() -> LaunchRouting {
    let viewController = RootViewController()
    let component = RootComponent(dependency: dependency,
                                  rootViewController: viewController)
    let interactor = RootInteractor(presenter: viewController)

    let playBuilder = PlayBuilder(dependency: component)
    return RootRouter(interactor: interactor,
                      viewController: viewController,
                      playBuilder: playBuilder)
  }
}
