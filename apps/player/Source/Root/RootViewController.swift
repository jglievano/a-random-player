import UIKit
import Viper

protocol RootPresentableListener: class {}

final class RootViewController: UIViewController, RootPresentable {
  weak var listener: RootPresentableListener?

  init() {
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("Method is not supported")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .white
  }
}

// MARK: - RootViewControllable

extension RootViewController: RootViewControllable {
  func present(viewController: ViewControllable) {
    present(viewController.uiviewController, animated: true, completion: nil)
  }
}

// MARK: - PlayViewControllable

extension RootViewController: PlayViewControllable {
  func dismiss(viewController: ViewControllable) {
    if presentedViewController === viewController.uiviewController {
      dismiss(animated: true, completion: nil)
    }
  }
}
