import UIKit

import RxSwift
import SnapKit

import HSUIKit
import Viper

protocol PlayPresentableListener: class {
  func togglePlay()
}

final class PlayViewController: UIViewController, PlayPresentable, PlayViewControllable {
  weak var listener: PlayPresentableListener?

  init() {
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("Method is not supported")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = UIColor.Headspace.lightBeige
    buildPlayButton()
    buildTimerLabel()
  }

  // MARK: - PlayPresentale

  func updateTimer(with seconds: TimeInterval) {
    let interval = Int(seconds)
    let seconds = interval % 60
    let minutes = (interval / 60) % 60
    let hours = (interval / 3600)
    timerLabel.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
  }

  // MARK: - Private

  private let bag = DisposeBag()

  private let playButton: ToggableButton = {
    let button = ToggableButton()
    button.setToggleEvents(
      untoggled: { button in
        button.backgroundColor = .black
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Play", for: .normal)
      },
      toggled: { button in
        button.backgroundColor = .gray
        button.setTitleColor(.black, for: .normal)
        button.setTitle("Pause", for: .normal)
      })

    return button
  }()

  private let timerLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .center
    label.text = "00:00:00"
    return label
  }()

  private func buildPlayButton() {
    view.addSubview(playButton)
    playButton.snp.makeConstraints { make in
      make.bottom.equalTo(view.safeAreaLayoutGuide).offset(-20)
      make.left.equalTo(view).offset(20)
      make.right.equalTo(view).offset(-20)
      make.height.equalTo(50)
    }
    playButton.rx.tap
        .subscribe(onNext: { [unowned self] in self.listener?.togglePlay() })
        .disposed(by: bag)
  }

  private func buildTimerLabel() {
    view.addSubview(timerLabel)
    timerLabel.snp.makeConstraints { make in
      make.bottom.equalTo(playButton.snp.top).offset(-20)
      make.left.equalTo(view).offset(20)
      make.right.equalTo(view).offset(-20)
      make.height.equalTo(50)
    }
  }
}
