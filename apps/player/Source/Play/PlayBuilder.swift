import Viper

protocol PlayDependency: Dependency {}

final class PlayComponent: Component<PlayDependency> {}

// MARK: - Builder

protocol PlayBuildable: Buildable {
  func build(with listener: PlayListener) -> PlayRouting
}

final class PlayBuilder: Builder<PlayDependency>, PlayBuildable {
  override init(dependency: PlayDependency) {
    super.init(dependency: dependency)
  }

  func build(with listener: PlayListener) -> PlayRouting {
    let viewController = PlayViewController()
    let interactor = PlayInteractor(presenter: viewController)
    interactor.listener = listener
    return PlayRouter(interactor: interactor, viewController: viewController)
  }
}
