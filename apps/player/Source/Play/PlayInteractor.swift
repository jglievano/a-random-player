import Foundation

import RxSwift

import Viper

protocol PlayRouting: ViewableRouting {}

protocol PlayPresentable: Presentable {
  var listener: PlayPresentableListener? { get set }
  func updateTimer(with seconds: TimeInterval)
}

protocol PlayListener: class {}

final class PlayInteractor: PresentableInteractor<PlayPresentable>,
    PlayInteractable {
  weak var router: PlayRouting?
  weak var listener: PlayListener?

  override init(presenter: PlayPresentable) {
    super.init(presenter: presenter)
    presenter.listener = self
  }

  override func didBecomeActive() {
    super.didBecomeActive()
    player = Player(with: "pack_part_") { [unowned self] state in
      guard let state = state else { return }
      self.presenter.updateTimer(with: state.currentTime)
    }
  }

  override func willResignActive() {
    super.willResignActive()
    player?.stop()
  }

  // MARK: - Private

  private var player: Player?
}

extension PlayInteractor: PlayPresentableListener {
  func togglePlay() {
    player?.toggle()
  }
}
