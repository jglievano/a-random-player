import Viper

protocol PlayInteractable: Interactable {
  var router: PlayRouting? { get set }
  var listener: PlayListener? { get set }
}

protocol PlayViewControllable: ViewControllable {}

final class PlayRouter: ViewableRouter<PlayInteractable, PlayViewControllable>,
    PlayRouting {
  override init(interactor: PlayInteractable, viewController: PlayViewControllable) {
    super.init(interactor: interactor, viewController: viewController)
    interactor.router = self
  }
}
