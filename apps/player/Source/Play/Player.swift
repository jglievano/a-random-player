import AVFoundation
import Foundation
import MediaPlayer

class AudioTimelineSegment {
  let duration: TimeInterval
  let plasticity: Double
  let priority: Double

  init(duration: TimeInterval, plasticity: Double, priority: Double) {
    assert(plasticity >= 0.0 && plasticity <= 1.0)
    assert(priority >= 0.0 && priority <= 1.0)
    self.duration = duration
    self.plasticity = plasticity
    self.priority = priority
  }

  func setStartTime(_ time: TimeInterval) {
    startTime = time
  }

  var startTime: TimeInterval = 0
}

class AudioTimeline {
  static let baseGap: TimeInterval = 3

  let base: String
  let duration: TimeInterval
  let segments: [AudioTimelineSegment]

  var numOfSegments: Int {
    return segments.count
  }

  var filledDuration: TimeInterval {
    let audioFilledDuration = segments
      .map { $0.duration }
      .reduce(0, { $0 + $1 })
    return audioFilledDuration + TimeInterval(numOfSegments - 1) * AudioTimeline.baseGap
  }

  var emptyDuration: TimeInterval {
    return duration - filledDuration
  }

  init(base: String, duration: TimeInterval, segments: [AudioTimelineSegment]) {
    self.base = base
    self.duration = duration
    self.segments = segments
  }

  func segmentUrl(at index: Int) -> URL? {
    let path = String(format: "%@%03d", base, index + 1)
    let url = Bundle.main.url(forResource: path, withExtension: "mp3")
    return url
  }

  func calculateStartTimes() {
    var timelinePos: TimeInterval = 0
    let totalPlasticity = segments.map { $0.plasticity }.reduce(0, { $0 + $1 })
    let factor = emptyDuration / totalPlasticity
    for segment in segments {
      segment.setStartTime(timelinePos)
      timelinePos +=
          AudioTimeline.baseGap + segment.duration + (segment.plasticity * factor)
    }
  }
}

class Player: NSObject {
  enum Activity {
    case stopped
    case playing
    case paused
  }

  struct State {
    var currentTime: TimeInterval
    var activity: Activity
  }

  init(with base: String, update: @escaping (State?) -> Void) {
    self.update = update
    self.state = State(currentTime: 0, activity: .stopped)
    timeline = AudioTimeline(
      base: base,
      duration: 10 * 60, // 10 minutes
      segments: [
        AudioTimelineSegment(duration: 93, plasticity: 0.0, priority: 1.0),
        AudioTimelineSegment(duration: 23, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 3, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 11, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 3, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 5, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 15, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 11, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 4, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 15, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 2, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 6, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 14, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 14, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 22, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 20, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 9, plasticity: 1.0, priority: 1.0),
        AudioTimelineSegment(duration: 32, plasticity: 1.0, priority: 1.0)
      ]
    )
    timeline.calculateStartTimes()

    do {
      try AVAudioSession.sharedInstance().setCategory(.playback,
                                                      mode: .default,
                                                      options: [.mixWithOthers, .allowAirPlay])
      try AVAudioSession.sharedInstance().setActive(true)
    } catch {
      print("ERROR AVAudioSession setup failed")
    }

    super.init()

    commandCenter.playCommand.addTarget { [unowned self] _ in
      if self.state.activity != .playing {
        self.play()
        return .success
      }
      return .commandFailed
    }

    commandCenter.pauseCommand.addTarget { [unowned self] _ in
      if self.state.activity == .playing {
        self.pause()
        return .success
      }
      return .commandFailed
    }
  }

  func toggle() {
    switch state.activity {
    case .stopped, .paused:
      play()
    case .playing:
      pause()
    }
  }

  func pause() {
    audioPlayer.stop()
    state.activity = .paused
    timer?.invalidate()
    timer = nil
    audioTimer?.invalidate()
    audioTimer = nil
  }

  func play() {

    if state.activity == .paused {
      audioPlayer.play()
    } else {
      scheduleNextSegment()
    }
    if let timer = timer {
        timer.invalidate()
    }
    self.timer = Timer.scheduledTimer(withTimeInterval: Constants.timerInterval,
                                      repeats: true) { [weak self] _ in
      guard let strongSelf = self else { return }
      strongSelf.forward(interval: Constants.timerInterval)
      strongSelf.setupNowPlaying()
    }
    state.activity = .playing
  }

  func stop() {
    audioPlayer.pause()
    state.activity = .stopped
    state.currentTime = 0
    timer?.invalidate()
    timer = nil
    audioTimer?.invalidate()
    audioTimer = nil
  }

  // MARK: - Private

  private struct Constants {
    static let timerInterval: TimeInterval = 0.05
  }

  private let commandCenter = MPRemoteCommandCenter.shared()
  private var audioPlayer = AVAudioPlayer()
  private var timer: Timer?
  private var audioTimer: Timer?
  private let timeline: AudioTimeline
  private var state: State
  private var update: (State?) -> Void
  private var loadedSegment: Int = 0

  private func forward(interval: TimeInterval) {
    state.currentTime += interval
    notify()
  }

  private func loadSegment(at index: Int) {
    guard let url = timeline.segmentUrl(at: index) else { return }
    print("INFO Loading segment at \(index)")
    do {
      audioPlayer = try AVAudioPlayer(contentsOf: url)
      audioPlayer.delegate = self
    } catch {
      print("ERROR \(error)")
    }
  }

  private func notify() {
    update(state)
  }

  private func scheduleNextSegment() {
    if let timer = audioTimer {
        timer.invalidate()
    }

    loadedSegment = timeline.segments.firstIndex(where: { $0.startTime >= state.currentTime }) ?? 0
    loadSegment(at: loadedSegment)
    let waitInterval = timeline.segments[loadedSegment].startTime - state.currentTime
    audioTimer = Timer.scheduledTimer(withTimeInterval: waitInterval + (loadedSegment == 0 ? 0 : 3),
                                      repeats: false) { [weak self] _ in
      guard let strongSelf = self else { return }
      print(String(format: "INFO Play segment at %.2f secs",
                   strongSelf.state.currentTime))
      strongSelf.audioPlayer.play()
    }
    print(String(format: "INFO Segment %d scheduled in %.2f secs", loadedSegment, waitInterval))
  }

  func setupNowPlaying() {
    var nowPlayingInfo = [String : Any]()
    nowPlayingInfo[MPMediaItemPropertyTitle] = "Headspace"

    if let image = UIImage(named: "hs-logo") {
      nowPlayingInfo[MPMediaItemPropertyArtwork] =
        MPMediaItemArtwork(boundsSize: image.size) { size in
          return image
        }
    }
    nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = state.currentTime
    nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = timeline.duration
    nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = audioPlayer.rate

    MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
  }
}

extension Player: AVAudioPlayerDelegate {
  func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
    if flag {
      notify()
    } else {
      update(nil)
    }
    if loadedSegment < timeline.numOfSegments - 1 {
      scheduleNextSegment()
    } else {
      stop()
    }
  }
}
