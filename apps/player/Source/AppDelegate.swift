import UIKit
import Viper

@UIApplicationMain
class AppDelegate: UIResponder {
  var window: UIWindow?

  func afterLaunchSetup() {
    let window = UIWindow(frame: UIScreen.main.bounds)
    self.window = window

    let launchRouter = RootBuilder(dependency: AppComponent()).build()
    self.launchRouter = launchRouter
    launchRouter.launch(from: window)
  }

  // MARK: - Private

  private var launchRouter: LaunchRouting?
}

extension AppDelegate: UIApplicationDelegate {
  func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    afterLaunchSetup()
    return true
  }
  func applicationWillResignActive(_ application: UIApplication) {}
  func applicationDidEnterBackground(_ application: UIApplication) {}
  func applicationDidBecomeActive(_ application: UIApplication) {}
  func applicationWillTerminate(_ application: UIApplication) {}
}
