import UIKit

extension UIColor {
    public struct Headspace {
        public static let lightBeige = UIColor(red: 0.98, green: 0.97, blue: 0.95, alpha: 1.0)
    }
}
