import UIKit

public typealias ToggleButtonClosure = (ToggableButton) -> Void
public enum ToggableButtonState {
  case untoggled
  case toggled
}

/// ToggableButton toggles appearence between two states.
public class ToggableButton: UIButton {
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  public required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  public func setToggleEvents(untoggled: @escaping ToggleButtonClosure, toggled: @escaping ToggleButtonClosure) {
    stateActions[.untoggled] = untoggled
    stateActions[.toggled] = toggled
    toggle(to: toggleState)
  }

  public func toggle() {
    switch toggleState {
    case .untoggled:
      toggleState = .toggled
      guard let action = stateActions[.toggled] else { return }
      action(self)
    case .toggled:
      toggleState = .untoggled
      guard let action = stateActions[.untoggled] else { return }
      action(self)
    }
  }

  public func toggle(to newState: ToggableButtonState) {
    toggleState = newState
    guard let action = stateActions[toggleState] else { return }
    action(self)
  }

  // MARK: - Private

  private var toggleState = ToggableButtonState.untoggled
  private var stateActions = [ToggableButtonState: ToggleButtonClosure]()

  private func setup() {
    self.addTarget(self, action: #selector(self.touchUpInsideToggle), for: .touchUpInside)
  }

  @objc private func touchUpInsideToggle() {
    toggle()
  }
}
