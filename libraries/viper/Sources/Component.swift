import Foundation

/// The base class for all components.
///
/// A component defines private properties to its internal router, interactor, presenter and view
/// units, as well as public properties to its children.
///
/// A component subclass implementation should conform to child `Dependency` protocols, defined by
/// all of its immediate children.
open class Component<DependencyType>: Dependency {
  /// The dependency of this `Component`.
  public let dependency: DependencyType

  /// Initializer.
  ///
  /// - parameter dependency: The dependency of this component, usually provided by the parent.
  public init(dependency: DependencyType) {
    self.dependency = dependency
  }

  /// Used to create a shared dependency in your component subclass. Shared dependencies are
  /// retained and reused by the component. Each dependent asking for this dependency will receive
  /// the same instance while the component is alive.
  ///
  /// - note: Any shared dependency's constructor may not switch threads as this may cause deadlock.
  ///
  /// - parameter factory: The closure to construct the dependency.
  /// - returns: The instance.
  public final func shared<T>(function: String = #function, _ factory: () -> T) -> T {
    lock.lock()
    defer {
      lock.unlock()
    }

    // Additional nil coalescing is needed to mitigate a Swift bug appearing in Xcode 10.
    // (https://bugs.swift.org/browse/SR-8704)
    // Without this measure, calling `shared` from a function that returns an optional type will
    // always pass the check below and return nil if the instance is not initialized.
    if let instance = (sharedInstance[function] as? T?) ?? nil {
      return instance
    }

    let instance = factory()
    sharedInstance[function] = instance

    return instance
  }

  // MARK: - Private

  private var sharedInstance = [String: Any]()
  private let lock = NSRecursiveLock()
}

/// The special empty component.
open class EmptyComponent: EmptyDependency {
  /// Initializer.
  public init() {}
}
