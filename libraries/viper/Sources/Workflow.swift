import RxSwift

/// Defines the base class for a sequence of steps that execute a flow through the application tree.
///
/// At each step of a workflow is a pair of value and actionable items. The value can be used to
/// make logic decisions. The actionable item is invoked to perform logic for the step. Typically
/// the actionable item is the interactor.
///
/// A workflow should always start at the root of the tree.
open class Workflow<ActionableItemType> {
  /// Called when the last step observable is completed.
  ///
  /// Subclasses should override this method if they want to execute logic at this point in the
  /// workflow lifecycle. The default implementation does nothing.
  open func didComplete() {}

  /// Called when the workflow is forked.
  ///
  /// Subclasses should override this method if they want to execute logic at this point in the
  /// workflow lifecycle. The default implementation does nothing.
  open func didFork() {}

  /// Called when the last step observable has error.
  ///
  /// Subclasses should override this method if they want to execute logic at this point in the
  /// workflow lifecycle. The default implementation does nothing.
  open func didReceiveError(_ error: Error) {}

  /// Initializer.
  public init() {}

  /// Execute the given closure as the root step.
  ///
  /// - parameter onStep: The closure to execute for the root step.
  /// - returns: The next step.
  public final func onStep<NextItemType, NextValueType>(
      _ onStep: @escaping (ActionableItemType) -> Observable<(NextItemType, NextValueType)>
  ) -> Step<ActionableItemType, NextItemType, NextValueType> {
    return Step(workflow: self, observable: subject.asObservable().take(1))
      .onStep { (actionableItem: ActionableItemType, _) in onStep(actionableItem) }
  }

  /// Subscribe and start the workflow sequence.
  ///
  /// - parameter actionableItem: The initial actionable item for the first step.
  /// - returns: The disposable of this workflow.
  public final func subscribe(_ actionableItem: ActionableItemType) -> Disposable {
    guard compositeDisposable.count > 0 else {
      assertionFailure("Attempt to subscribe to \(self) before it is commited.")
      return Disposables.create()
    }

    subject.onNext((actionableItem, ()))
    return compositeDisposable
  }

  // MARK: - Private

  private let subject = PublishSubject<(ActionableItemType, ())>()
  private var didInvokeComplete = false

  /// The composite disposable that contains all subscriptions including the original workflow as
  /// well as all the forked ones.
  fileprivate let compositeDisposable = CompositeDisposable()

  fileprivate func didCompleteIfNotYet() {
    // Since a workflow may be forked to produce multiple subscribed Rx chains, we should ensure the
    // didComplete method is only invoked once per workflow instance. See `Step.commit` on why the
    // side effects must be added at the end of the Rx chains.
    guard !didInvokeComplete else { return }
    didInvokeComplete = true
    didComplete()
  }
}

/// Defines a single step in a workflow.
///
/// A step may produce a next step with a new value and actionable item, eventually forming a
/// sequence of workflow steps.
///
/// Steps are asynchronous by nature.
open class Step<WorkflowItemType, ActionableItemType, ValueType> {
  private let workflow: Workflow<WorkflowItemType>
  private var observable: Observable<(ActionableItemType, ValueType)>

  fileprivate init(workflow: Workflow<WorkflowItemType>,
                   observable: Observable<(ActionableItemType, ValueType)>) {
    self.workflow = workflow
    self.observable = observable
  }

  /// Executes the given closure for this step.
  ///
  /// - parameter onStep: The closure to execute for the step.
  /// - returns: The next step.
  public final func onStep<NextItemType, NextValueType>(
      _ onStep: @escaping (ActionableItemType,
                           ValueType) -> Observable<(NextItemType, NextValueType)>
  ) -> Step<WorkflowItemType, NextItemType, NextValueType> {
    let confinedNextStep = observable
      .flatMapLatest { (actionableItem, value) -> Observable<(Bool,
                                                              ActionableItemType,
                                                              ValueType)> in
        // We cannot use generic constraint here since Swift requires constraints be satisfied by
        // concrete types, preventing using protocol as actionable type.
        if let interactor = actionableItem as? Interactable {
          return interactor
            .isActiveStream
            .map({ (isActive: Bool) -> (Bool, ActionableItemType, ValueType) in
              (isActive, actionableItem, value)
            })
        } else {
          return Observable.just((true, actionableItem, value))
        }
      }
      .filter { (isActive: Bool, _, _) -> Bool in isActive }
      .take(1)
      .flatMapLatest { (_, actionableItem: ActionableItemType, value: ValueType) ->
          Observable<(NextItemType, NextValueType)> in
        onStep(actionableItem, value)
      }
      .take(1)
      .share()

    return Step<WorkflowItemType,
                NextItemType,
                NextValueType>(workflow: workflow, observable: confinedNextStep)
  }

  /// Executes the given closure when the step produces an error.
  ///
  /// - parameter onError: The closure to execute when an error occurs.
  /// - returns: This step.
  public final func onError(
      _ onError: @escaping ((Error) -> Void)
  ) -> Step<WorkflowItemType, ActionableItemType, ValueType> {
    observable = observable.do(onError: onError)
    return self
  }

  /// Commit the steps of the workflow sequence.
  ///
  /// - returns: The commited workflow.
  @discardableResult
  public final func commit() -> Workflow<WorkflowItemType> {
    // Side-effects must be chained at the last observable sequence, since errors and complete
    // events can be emitted by any observables on any steps of the workflow.
    let disposable = observable
      .do(onError: workflow.didReceiveError, onCompleted: workflow.didCompleteIfNotYet)
      .subscribe()
    _ = workflow.compositeDisposable.insert(disposable)
    return workflow
  }

  /// Convert the workflow into an observable.
  ///
  /// - returns: The observable representation of this workflow.
  public final func asObservable() -> Observable<(ActionableItemType, ValueType)> {
    return observable
  }
}

/// Workflow related observable extensions.
public extension ObservableType {
  /// Fork the step from this observable.
  ///
  /// - parameter workflow: The workflow this step belongs to.
  /// - returns: The newly forked step in the workflow. `nil` if this observable does not conform to
  ///   the required generic type of (ActionableItemType, ValueType).
  func fork<WorkflowItemType, ActionableItemType, ValueType>(
      _ workflow: Workflow<WorkflowItemType>
  ) -> Step<WorkflowItemType, ActionableItemType, ValueType>? {
    if let stepObservable = self as? Observable<(ActionableItemType, ValueType)> {
      workflow.didFork()
      return Step(workflow: workflow, observable: stepObservable)
    }
    return nil
  }
}

/// Workflow related disposable extensions.
public extension Disposable {
  /// Dispose the subscription when the given workflow is disposed.
  ///
  /// when using this composition, the subscription closure may freely retain the workflow itself,
  /// since the subscription closure is disposed once the workflow is disposed, thus releasing the
  /// retain cycle before the workflow needs to be deallocated.
  ///
  /// - note: This is the preferred method when trying to confine a subscription to the lifecycle of
  ///   a workflow.
  ///
  /// - parameter workflow: The workflow to dispose the subscription with.
  func disposeWith<ActionableItemType>(workflow: Workflow<ActionableItemType>) {
    _ = workflow.compositeDisposable.insert(self)
  }
}
