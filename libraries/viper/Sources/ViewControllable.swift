import UIKit

/// Basic interface between a router and the `UIViewController`.
public protocol ViewControllable: class {
  var uiviewController: UIViewController { get }
}

/// Default implementation on `UIViewController` to conform `ViewControllable` protocol.
public extension ViewControllable where Self: UIViewController {
  var uiviewController: UIViewController { return self }
}
