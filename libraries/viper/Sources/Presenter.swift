import Foundation

/// The base protocol for all presenters.
public protocol Presentable: class {}

/// The base class of all presenters. A `presenter` translates business models into values the
/// corresponding `ViewController` can consume and display. It also maps UI events to business logic
/// method, invoked to its listener.
open class Presenter<ViewControllerType>: Presentable {
  /// The view controller of this presenter.
  public let viewController: ViewControllerType

  /// Initializer.
  ///
  /// - parameter viewController: The `ViewController` of this `Presenter`.
  public init(viewController: ViewControllerType) {
    self.viewController = viewController
  }
}
