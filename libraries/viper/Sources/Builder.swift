import Foundation

/// The base builder protocol that all builders should conform to.
public protocol Buildable: class {}

/// Utility that instantiates a component and sets up its internal wirings.
open class Builder<DependencyType>: Buildable {
  /// The dependency used for this builder to build the component.
  public let dependency: DependencyType

  /// Initializer.
  ///
  /// - parameter dependency: The dependency used for this builder to build the component.
  public init(dependency: DependencyType) {
    self.dependency = dependency
  }
}
