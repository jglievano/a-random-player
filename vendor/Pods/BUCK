COMPILER_FLAGS = ["-w"]
SWIFT_COMPILER_FLAGS = ["-suppress-warnings"]

apple_library(
    name = "RxAtomic",
    modular = True,
    compiler_flags = COMPILER_FLAGS,
    swift_compiler_flags = SWIFT_COMPILER_FLAGS,
    exported_headers = glob(["RxAtomic/**/*.h"]),
    srcs = glob(["RxAtomic/**/*.c"]),
)

apple_library(
    name = "RxSwift",
    modular = True,
    compiler_flags = COMPILER_FLAGS,
    swift_compiler_flags = SWIFT_COMPILER_FLAGS,
    preprocessor_flags = ["-fobjc-arc"],
    srcs = glob([
        "RxSwift/Platform/**/*.swift",
        "RxSwift/RxSwift/**/*.swift",
    ]),
    deps = [":RxAtomic"],
    visibility = ["PUBLIC"],
)

apple_library(
    name = "RxCocoa",
    compiler_flags = COMPILER_FLAGS,
    swift_compiler_flags = SWIFT_COMPILER_FLAGS,
    preprocessor_flags = ["-fobjc-arc"],
    bridging_header = "RxCocoa/RxCocoa/RxCocoa-Bridging-Header.h",
    exported_headers = glob(["RxCocoa/**/*.h"]),
    srcs = glob([
        "RxCocoa/Platform/**/*.swift",
        "RxCocoa/RxCocoa/**/*.m",
        "RxCocoa/RxCocoa/**/*.swift",
    ]),
    deps = [":RxSwift"],
    visibility = ["PUBLIC"],
)

apple_library(
    name = "SnapKit",
    modular = True,
    srcs = glob(["SnapKit/Source/*.swift"]),
    visibility = ["PUBLIC"],
)

apple_library(
    name = "IGListKit",
    preprocessor_flags = ["-fobjc-arc"],
    exported_headers = glob([
        "IGListKit/Source/**/*.h",
    ], exclude = [
        "IGListKit/Source/Internal/*.h",
        "IGListKit/Source/Common/Internal/*.h",
    ]),
    headers = glob([
        "IGListKit/Source/Internal/*.h",
        "IGListKit/Source/Common/Internal/*.h",
    ]),
    visibility = ["PUBLIC"],
)
