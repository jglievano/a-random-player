SHARED_CONFIGS = {
    "IPHONEOS_DEPLOYMENT_TARGET": "11.0",
}

def bundle_identifier(name):
    print(name)
    return "com.getsomeheadspace.%s" % name

def test_configs():
    test_specific_config = {
        "SWIFT_WHOLE_MODULE_OPTIMIZATION": "YES",
    }
    test_config = SHARED_CONFIGS + test_specific_config
    configs = {
        "Debug": test_config,
        "Profile": test_config,
    }
    return configs

def library_configs():
    lib_specific_config = {
        "SWIFT_WHOLE_MODULE_OPTIMIZATION": "YES",
    }
    library_config = SHARED_CONFIGS + lib_specific_config
    configs = {
        "Debug": library_config,
        "Profile": library_config,
    }
    return configs

def binary_configs(name):
    binary_specific_config = {
        "ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES": "YES",
        "DEVELOPMENT_LANGUAGE": "Swift",
        "PRODUCT_BUNDLE_IDENTIFIER": bundle_identifier(name),
    }
    binary_config = SHARED_CONFIGS + binary_specific_config
    configs = {
        "Debug": binary_config,
        "Profile": binary_config,
    }
    return configs

def info_plist_substitutions(name):
    substitutions = {
        "DEVELOPMENT_LANGUAGE": "en-us",
        "EXECUTABLE_NAME": name,
        "PRODUCT_BUNDLE_IDENTIFIER": bundle_identifier(name),
        "PRODUCT_NAME": name,
    }
    return substitutions
