#!/usr/bin/env bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR=${BASE_DIR%/scripts}
TOOLS_DIR="${DIR}/tools"
BUCK_DIR="${TOOLS_DIR}/buck"
BUCK_TAR="${TOOLS_DIR}/buck.tar.gz"
BUCK_VERSION=$1
BUCK_URL="https://github.com/facebook/buck/releases/download/v${BUCK_VERSION}/buck-${BUCK_VERSION}.yosemite.bottle.tar.gz"

if [ -d "${BUCK_DIR}" ]; then
  printf "\e[1;31mBuck already installed.\e[0m\n"
  exit 0
fi
printf "\e[1mInstalling buck.\e[0m\n"
printf "  \e[35mTOOLS_DIR=\e[1m${TOOLS_DIR}\e[0m\n"
printf "  \e[35mBUCK_DIR=\e[1m${BUCK_DIR}\e[0m\n"
printf "  \e[35mBUCK_TAR=\e[1m${BUCK_TAR}\e[0m\n"
printf "  \e[35mBUCK_URL=\e[1m${BUCK_URL}\e[0m\n"

curl -o ${BUCK_TAR} -LS ${BUCK_URL}
tar -xf ${BUCK_TAR} --directory ${TOOLS_DIR}
rm ${BUCK_TAR}
mv ${BUCK_DIR}/${BUCK_VERSION}/* ${BUCK_DIR}
rm -rf ${BUCK_DIR}/${BUCK_VERSION}
printf "\e[1;32mDone.\e[0m\n"

