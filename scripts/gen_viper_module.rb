#!/usr/bin/env ruby

require 'fileutils'

def print_usage()
  puts
  puts "\e[1mNAME\e[0m"
  puts "    \e[1mgen_viper_module.rb\e[0m -- Generates a VIPER module."
  puts
  puts "\e[1mSYNOPSIS\e[0m"
  puts "    \e[1mgen_viper_module.rb\e[0m [\e[1m-v\e[0m] \e[4mmodule_name\e[0m \e[4mdst_path\e[0m"
  puts
  puts "\e[1mDESCRIPTION\e[0m"
  puts "    This utility generates a new VIPER module, obtaining its \e[4mmodule_name\e[0m in CamelCase."
  puts
  puts "    The following options are available:"
  puts
  puts "    \e[1m-v\e[0m    Specify that this VIPER module will have its own view."
  puts "    \e[1m-h\e[0m    No-op execution. Prints this."
  puts
  puts "\e[1mEXIT STATUS\e[0m"
  puts "    0 on success, and >0 if an error occurs."
  puts
  puts "\e[1mCOMPATIBILITY\e[0m"
  puts "    Built and tested with \e[4mRuby v2.5\e[0m."
  puts
end

def print_help_and_exit()
  puts "For help, run `\e[1mgen_viper_module.rb -h\e[0m`"
  exit
end

def copy_module_with_substitutions(module_name, src, dst)
  puts "\e[1mCopying\e[0m #{src} \e[32mto\e[0m #{dst}"
  downcase_module_name = module_name[0,1].downcase + module_name[1..-1]
  contents = File.read(src)
  contents.gsub!(/___Basename___/, module_name)
  contents.gsub!(/___basename___/, downcase_module_name)
  File.open(dst, "w") do |file|
    file.write(contents)
  end
end

if __FILE__ == $PROGRAM_NAME
  if ARGV.include? "-h"
    print_usage
    exit
  end

  template = "default"
  has_own_view = ARGV.index "-v"
  if has_own_view
    ARGV.delete_at has_own_view
    template = "with_view"
  end

  if ARGV.length != 2
    puts "\e[31mTwo (2) arguments required.\e[0m"
    print_help_and_exit
  end

  module_name = ARGV[0]
  dst_path = ARGV[1]

  puts "\e[1mGenerating VIPER module with its own view\e[0m" if has_own_view
  puts "\e[1mModule name:\e[0m #{module_name}"
  puts "\e[1mDestination path:\e[0m #{dst_path}"

  root_dir = File.expand_path "#{File.dirname(__FILE__)}/.."
  base_dir = "#{root_dir}/#{dst_path}"
  template_dir = "#{root_dir}/scripts/templates/viper/#{template}"
  unless Dir.exist? base_dir
    puts "\e[31m#{base_dir} does not exist.\e[0m"
    print_help_and_exit
  end
  module_dir = "#{base_dir}/#{module_name}"
  FileUtils.mkdir_p "#{module_dir}"
  cp_list = ["Builder", "Interactor", "Router"]
  cp_list += ["ViewController"] if has_own_view
  cp_list.each do |cp_item|
    cp_src = "#{template_dir}/___Basename___#{cp_item}.swift"
    cp_dst = "#{module_dir}/#{module_name}#{cp_item}.swift"
    copy_module_with_substitutions(module_name, cp_src, cp_dst)
  end
end
