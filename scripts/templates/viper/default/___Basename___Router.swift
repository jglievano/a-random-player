import Viper

protocol ___Basename___Interactable: Interactable {
  var router: ___Basename___Routing? { get set }
  var listener: ___Basename___Listener? { get set }
}

protocol ___Basename___ViewControllable: ViewControllable {}

final class ___Basename___Router: Router<___Basename___Interactable>,
    ___Basename___Routing {
  init(interactor: ___Basename___Interactable, viewController: ___Basename___ViewControllable) {
    self.viewController = viewController
    super.init(interactor: interactor)
    interactor.router = self
  }

  func cleanupViews() {}

  // MARK: - Private

  private let viewController: ___Basename___ViewControllable
}
