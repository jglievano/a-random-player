import Viper

protocol ___Basename___Routing: Routing {
  func cleanupViews()
}

protocol ___Basename___Listener: class {}

final class ___Basename___Interactor: Interactor,
    ___Basename___Interactable {
  weak var router: ___Basename___Routing?
  weak var listener: ___Basename___Listener?

  override init() {}

  override func didBecomeActive() {
    super.didBecomeActive()
  }

  override func willResignActive() {
    super.willResignActive()
    router?.cleanupViews()
  }
}
