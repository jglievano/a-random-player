import Viper

protocol ___Basename___Dependency: Dependency {
  var ___basename___ViewController: ___basename___ViewControllable { get }
}

final class ___Basename___Component: Component<___basename___Dependency> {
  fileprivate var ___basename___ViewController: ___basename___ViewControllable {
    return dependency.___basename___ViewController
  }
}

// MARK: - Builder

protocol ___Basename___Buildable: Buildable {
  func build(with listener: ___Basename___Listener) -> ___Basename___Routing
}

final class ___Basename___Builder: Builder<___Basename___Dependency>,
    ___Basename___Buildable {
  override init(dependency: ___Basename___Dependency) {
    super.init(dependency: dependency)
  }

  func build(with listener: ___Basename___Listener) -> ___Basename___Routing {
    let component = ___Basename___Component(dependency: dependency)
    let interactor = ___Basename___Interactor()
    interactor.listener = listener
    return ___Basename___Router(interactor: interactor, viewController: component.___basename___ViewController)
  }
}
