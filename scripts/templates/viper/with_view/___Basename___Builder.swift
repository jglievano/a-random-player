import Viper

protocol ___Basename___Dependency: Dependency {}

final class ___Basename___Component: Component<___Basename___Dependency> {}

// MARK: - Builder

protocol ___Basename___Buildable: Buildable {
  func build(with listener: ___Basename___Listener) -> ___Basename___Routing
}

final class ___Basename___Builder: Builder<___Basename___Dependency>,
    ___Basename___Buildable {
  override init(dependency: ___Basename___Dependency) {
    super.init(dependency: dependency)
  }

  func build(with listener: ___Basename___Listener) -> ___Basename___Routing {
    let component = ___Basename___Component(dependency: dependency)
    let viewController = ___Basename___ViewController()
    let interactor = ___Basename___Interactor(presenter: viewController)
    interactor.listener = listener
    return ___Basename___Router(interactor: interactor: viewController: viewController)
  }
}
