import Viper

protocol ___Basename___Interactable: Interactable {
  var router: ___Basename___Routing? { get set }
  var listener: ___Basename___Listener? { get set }
}

protocol ___Basename___ViewControllable: ViewControllable {}

final class ___Basename___Router: ViewableRouter<___Basename___Interactable, ___Basename___ViewControllable>,
    ___Basename___Routing {
  override init(interactor: ___Basename___Interactable, viewController: ___Basename___ViewControllable) {
    super.init(interactor: interactor, viewController: viewController)
    interactor.router = self
  }
}
