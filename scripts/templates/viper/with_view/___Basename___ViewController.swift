import UIKit

import Viper

protocol ___Basename___PresentableListener: class {}

final class ___Basename___ViewController: UIViewController,
    ___Basename___Presentable,
    ___Basename___ViewControllable {
  weak var listener: ___Basename___PresentableListener?
}
