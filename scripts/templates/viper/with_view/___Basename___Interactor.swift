import Viper

protocol ___Basename___Routing: ViewableRouting {}

protocol ___Basename___Presentable: Presentable {
  var listener: ___Basename___PresentableListener? { get set }
}

protocol ___Basename___Listener: class {}

final class ___Basename___Interactor: PresentableInteractor<___Basename___Presentable>,
    ___Basename___Interactable,
    ___Basename___PresentableListener {
  weak var router: ___Basename___Routing?
  weak var listener: ___Basename___Listener?

  override init(presenter: ___Basename___Presentable) {
    super.init(presenter: presenter)
    presenter.listener = self
  }

  override func didBecomeActive() {
    super.didBecomeActive()
  }

  override func willResignActive() {
    super.willResignActive()
  }
}
