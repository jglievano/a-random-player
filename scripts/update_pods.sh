#!/usr/bin/env bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DIR=${BASE_DIR%/scripts}
PODS_DIR="${DIR}/vendor/Pods"
KEEP_PATH="${PODS_DIR}/KEEP"

cd ${DIR}

# Copy files into a temporary file located at `./tmp`.
mkdir tmp
while IFS='' read -r f || [[ -n "$f" ]]; do
  cp ${PODS_DIR}/${f} tmp/
done < "${KEEP_PATH}"

cd ${DIR}
pod repo update
pod install
rm -rf Pods/{Headers,Local\ Podspecs,Pods.xcodeproj,Target\ Support\ Files}
find vendor/Pods -maxdepth 1 -mindepth 1 ! -name KEEP -print0 | xargs -0 rm -rf
mv Pods/* vendor/Pods/
rm -rf Pods

# Restore kept files.
while IFS='' read -r f || [[ -n "$f" ]]; do
  filename=$(basename "${f}")
  dir=$(dirname "${f}")
  mkdir -p ${PODS_DIR}/${dir}
  mv tmp/${filename} ${PODS_DIR}/${f}
done < "${KEEP_PATH}"
rm -rf tmp
